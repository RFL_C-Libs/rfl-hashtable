# RFL Hashtable

A basic hashtable utilizing the DJB2 hashing algorithm. 

**NOTE:** Currently only supports char* -> char*.

# Features 

- [x] logging via [rfl-levelled-logger](https://gitlab.com/RFL_C-Libs/rfl-levelled-logger)
- [x] c99
- [x] type portability (via inttypes.h)
- [x] pure C
- [ ] supports any* -> any*
- [ ] thread safety
- [ ] packed structs

# Install

To start using this library, you will need a hashtable.o file and the hashtable.h file.

If you intend to use the logger, you will also need the logger.o and logger.h files from [rfl-levelled-logger](https://gitlab.com/RFL_C-Libs/rfl-levelled-logger).

**NOTE**: I use clang and therefore so does the Makefile. You could use gcc all the same.

1. Clone the repo
2. `cd rfl-hashtable`
3. `make`
    - If Make is not availabe on your machine, you can compile the library manually: `clang -c -std=c99 -O2 hashtable.c`
4. Copy hashtable.h, hashtable.o into your source directory and `include "hashtable.h"` in your code.
5. If you intend to use the logger, follow the instructions at [rfl-levelled-logger](https://gitlab.com/RFL_C-Libs/rfl-levelled-logger).
5. When you are ready to compile, make sure you include hashtable.o (and logger.o, if applicable).
    - ex: `clang -o my_output_binary -std=c99 -Wall -gdwarf-4 -O0 my_code.c logger.o hashtable.o`

# Usage

## Example

```C
Hash h = NewHashWithLogger(30, NULL, LOGLEVELDEBUG);

Add(h, "Hello", "World"); // insert "Hello" -> "World"

char* val = Get(h, "Hello");

FreeHash(h);
```