#include "hashtable.h"
#include "logger.h"
#include <stdio.h>
#include <stdlib.h>

int main(){
    Hash h = NewHashWithLogger(30, NULL, LOGLEVELDEBUG);

    printf("Adding 'Hello' -> 'World'\n");
    Add(h, "Hello", "World");
    printf("Getting 'Hello'\n");
    char* v = Get(h, "Hello");
    printf("Got 'Hello' -> '%s'\n", v);
    char** d = Dump(h);
    
    // free the hash dump as we print it
    for (int i = 0; i < 30; i++){
        printf("%s\n", d[i]);
        free(d[i]);
    }
    free(d);

    Add(h, "second added key", "2");
    printf("Got 'second added key' -> '%s'\n", Get(h, "second added key"));

    FreeHash(h);
}