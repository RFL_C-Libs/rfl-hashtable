/**
 * @file hashtable.h
 * @author R Landau (rflandau@pm.me)
 * @brief header for the shared hashtable. 
 * Items are stored by their hashed key in the array, pointing to a K-V item
 * storing the original (unhashed) key and the value.
 * @version 0.8
 * @date 2024-01-08
 * 
 * @copyright Copyright (c) 2024
 * 
 */
#ifndef _HASHTABLE_H_INCLUDED_
#define _HASHTABLE_H_INCLUDED_

#include <stdint.h>

#include <inttypes.h>
#include "logger.h"

// Exported type ###############################################################

typedef struct RFLHashObj* Hash;

// Constructors ################################################################

/**
 * @brief Creates a new hashtable.
 * 
 *  Creates a new hashtable with the given capacity.
 *
 * @param cap max number of elements in the hashtable
 * @return Hash 
 */
Hash NewHash(uint32_t cap);

/**
 * @brief Creates a new hashtable with an internal logger.
 * 
 * Creates a hashtable with the given capacity and an internal RFL logger.
 * This logger cannot be configurated further than setting the log level.
 * You must include Logger.o from RFL Logger.
 * 
 * @param cap max number of elements in the hashtable
 * @param log_output_path path for the internal logger to print to
 * @param lev debug level (enumerated in logger.h) 
 * @return Hash 
 */
Hash NewHashWithLogger(uint32_t cap, char* log_output_path, enum level lev);

// Destructors #################################################################

/**
 * @brief Releases all memory held by h.
 * 
 * @param h the hashtable to free
 */
void FreeHash(Hash h);

/**
 * @brief Inserts the given K-V. char* -> char*
 * 
 * @param h the hash
 * @param key k
 * @param val v
 * @return char* | Returns an error string on failure, NULL on success.
 */
char* Add(Hash h, char* key, char* val);

/**
 * @brief Deletes and frees the given key and its associated value.
 * Does nothing if the key was unknown.
 * 
 * @param h the hash
 * @param key k
 * @return int | Returns 0 if no values were changed, 1 otherwise.
 */
uint16_t Del(Hash h, char* key);
/**
 * @brief Returns the value associated with the given key.
 * 
 * @param h the hash
 * @param key k
 * @return char* | value or NULL
 */
char* Get(Hash h, char* key);

/**
 * @brief Returns a pre-formatted string of all K-Vs in the hashtable.
 *  Remember to free the returned string!
 * 
 * Random order, based on the hash in the internal array.
 * @param h the hash
 * @return char* 
 */
char** Dump(Hash h);

#endif