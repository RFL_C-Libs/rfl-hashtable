TEST_HASHTABLE_BINARY=test_hashtable
TEST_ITEMLIST_BINARY=test_itemlist
GENERATED_OBJECT_FILES=itemlist.o hashtable.o
SOURCE_FILES := itemlist.c hashtable.c

all : $(GENERATED_OBJECT_FILES)

lib_prod : 
	clang -c hashtable.o -std=c99 -O2 $(SOURCE_FILES) logger.o

test_hashtable : hashtable.o $(TEST_HASHTABLE_BINARY).c
	clang -o $(TEST_HASHTABLE_BINARY) -std=c99 -Wall -Wextra -Wconversion -gdwarf-4 -O0 -lefence test.c $(GENERATED_OBJECT_FILES) logger.o
	valgrind --leak-check=full --track-origins=yes -s ./$(TEST_HASHTABLE_BINARY)

memtest_itemlist : itemlist.o $(TEST_ITEMLIST_BINARY).c
	clang -o $(TEST_ITEMLIST_BINARY) -std=c99 -Wall -Wextra -Wconversion -gdwarf-4 -O0 -lefence test_itemlist.c itemlist.o logger.o
	valgrind --leak-check=full --track-origins=yes -s ./$(TEST_ITEMLIST_BINARY)

test_itemlist : itemlist.o $(TEST_ITEMLIST_BINARY).c
	clang -o $(TEST_ITEMLIST_BINARY) -std=c99 -Wall -Wextra -Wconversion -gdwarf-4 -O0 -lefence test_itemlist.c itemlist.o logger.o
	./$(TEST_ITEMLIST_BINARY)

debug_itemlist : itemlist.o $(TEST_ITEMLIST_BINARY).c
	clang -o $(TEST_ITEMLIST_BINARY) -std=c99 -Wall -Wextra -Wconversion -gdwarf-4 -O0 -lefence test_itemlist.c itemlist.o logger.o
	gdb ./$(TEST_ITEMLIST_BINARY)

itemlist.o : itemlist.c logger.o
	clang -c -std=c99 -Wall -Wextra -Wconversion -gdwarf-4 -O0 itemlist.c

hashtable.o : hashtable.c logger.o
	clang -c -std=c99 -Wall -Wextra -Wconversion -gdwarf-4 -O0 hashtable.c

clean : 
	rm -f $(GENERATED_OBJECT_FILES) $(TEST_HASHTABLE_BINARY) $(TEST_ITEMLIST_BINARY)

doc :
	doxygen