/**
 * @file itemlist.h
 * @author R Landau (rflandau@pm.me)
 * @brief Internal handling for hashtable collisions.
 *  Covers all processes for interacting with a single index in the hashtable.
 *  Not intended to included in anything other than RFLHash.
 * @version 1.0
 * @date 2024-01-08
 * 
 * @copyright Copyright (c) 2024
 * 
 */

#ifndef _ITEMLIST_H_INCLUDED_
#define _ITEMLIST_H_INCLUDED_

#include "logger.h"
#include <inttypes.h>

// Exported Type ###############################################################

typedef struct RFLItemList* ItemList;

// Constructors ################################################################

/**
 * @brief Creates a new list of k-vs
 * 
 * @return ItemList 
 */
ItemList NewItemList();

/**
 * @brief 
 * 
 * @param log_output_path 
 * @param lev 
 * @return ItemList 
 */
ItemList NewItemListWithLogger(char* log_output_path, enum level lev);

// Destructors -----------------------------------------------------------------

/**
 * @brief Frees the entire list and every node therein.
 * 
 * @param il item list
 * @param free_logger if 0, the logger is not freed
 */
void FreeItemList(ItemList il, uint16_t free_logger);

// Subroutines -----------------------------------------------------------------

/**
 * @brief Searches for the given k-v in the item list
 * 
 * @param il item list
 * @param key key to search for
 * @param copy whether or not the value should be copied.
        if false, this char* will point to the internal value and thus should not be freed for the life of the node
 * @return value associated to key or NULL if not found 
 */
char* ItemList_Get(ItemList il, char* key, uint16_t copy);

/**
 * @brief Deletes the specified k node from the item list.
 *
 * @note deletes the *first instance* of the key it finds
 * 
 * @param il item list
 * @param key k
 * @return int | Returns 0 if no values were changed, 1 otherwise.
 */
uint16_t ItemList_Del(ItemList il, char* key);

/**
 * @brief Creates and appends a new node to the given list using the provided key and value.
 * 
 * @param il item list
 * @param key key
 * @param value value
 * @return new length 
 */
uint32_t ItemList_Append(ItemList il, char* key, char* value);

/**
 * @brief length of the given itemlist
 * 
 * @param il item list
 * @return length 
 */
uint32_t ItemList_Length(ItemList il);

/**
 * @brief Returns an array of pre-formatted strings, one per node.
 *  Remember to free the returned strings!
 * 
 * @param il item list
 * @return char** 
 */
char** ItemList_Dump(ItemList il);

#endif
 