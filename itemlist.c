#include <stdint.h>
#include <stdio.h> // snprintf
#include <stdlib.h>
#include <string.h>
#include "itemlist.h"
#include "logger.h"


// Hashtable Item List Node ####################################################

/**
 * @brief A node contains a k-v associated to the hash and links to adjacent nodes.
 * 
 */
typedef struct RFLHashItemListNode{
    struct RFLHashItemListNode* next; /**< next node*/
    struct RFLHashItemListNode* prev; /**< prev node*/
    char* key;
    char* value;
} RFLHashItemListNode;
typedef RFLHashItemListNode* Node;

/**
 * @brief Creates a new node k-v node that can be doubly linked
 * 
 * @param key key to be *copied* into the new node 
 * @param value value to be *copied* into the new node
 * @param prev node linked behind this node
 * @param next node linked ahead of this node
 * @return Node 
 */
Node newNode(char* restrict key, char* restrict value, Node prev, Node next){
    Node n = malloc(sizeof(RFLHashItemListNode));

    n->key = malloc(strlen(key)+1); //include \0
    strcpy(n->key, key);

    n->value = malloc(strlen(value)+1); //include \0
    strcpy(n->value, value);

    n->next = next;
    n->prev = prev;

    return n;
}

/**
 * @brief frees the memory associated to the node.
 * 
 * @note does not link adjacent nodes! You should do so prior to destruction.
 * @param n 
 */
void freeNode(Node n){
    // junk the internal data we copied
    free(n->key);
    free(n->value);
    // null out links
    n->next = n->prev = NULL;
    free(n);
    n = NULL;
    return;
}

/**
 * @brief stringify the given node
 *
 * @note make sure you free the returned value!
 *
 * @param n 
 * @return char* | a one-line representation of the node
 */
char* stringNode(Node n){
    uint64_t bytes = 2 + // "<-" 
                64 + // prev pointer
                6 + // " | k: "
                strlen(n->key) + 
                strlen(" | ") +
                64 + // cur pointer
                6 + // " | v: "
                strlen(n->value) +
                3 + // " | "
                64 + // next pointer
                2; // "->"
                // TODO tighten allocation, need to determine how to strlen a ptr address
                // TODO add self pointer address

    char* s = malloc(bytes);
    snprintf(s, bytes, "<-%p | k: %s | %p | v: %s | %p->", n->prev, n->key, n, n->value, n->next);
    return s;
}

// Item List ###################################################################

/**
 * @brief ItemLists handle the collection of items associated to a hash value.
 * Basically, handles collisions so the hashtable doesn't have to.
 * Implemented as a doubly-linked list.
 * 
 * @note does not handle duplicate keys in any way. If using duplicate keys, see the note in Del
 */
typedef struct RFLItemList {
    Node head; /**< first node*/
    Node tail; /**< last node*/

    uint32_t length; /**< current number of nodes*/
    Logger l;
} RFLItemList;

// Constructors ----------------------------------------------------------------

ItemList initItemList(){
    ItemList hil = malloc(sizeof(RFLItemList));

    hil->head = NULL;
    hil->tail = NULL;

    hil->length = 0;

    return hil;
}

ItemList NewItemList(){
    ItemList il = initItemList();
    il->l = NULL;
    return il;
}

ItemList NewItemListWithLogger(char* log_output_path, enum level lev){
    Logger l = NewLoggerWithLevel(log_output_path, lev);
    SetLevel(l, lev);

    ItemList il = initItemList();
    il->l = l;
    Info(l, "itemlist initialized");
    return il;
}

// Destructors -----------------------------------------------------------------

void FreeItemList(ItemList il, uint16_t free_logger){
    Info(il->l, "destroying ItemList @ %p with length: %d", il, il->length);
    // start at the tail and free each node, working backwards
    Node cur_node = il->tail; uint32_t i = 0;
    Node following_target;
    while (cur_node != NULL){
        following_target = cur_node->prev; // freeing a node nils its adjacency, so we need to capture its neighbours beforehand
        freeNode(cur_node);
        cur_node = following_target;
        i += 1;// track index so we know, retrospectively if the length was out of sync
    }
    if(i != il->length) Warn(il->l, "ItemList length (%d) <> actual node chain count (index: %d). ", il->length, i);
    il->head = NULL;
    il->tail = NULL;
    il->length = 0;
    if (free_logger) FreeLogger(il->l);
    free(il);
}

// Subroutines -----------------------------------------------------------------

char* ItemList_Get(ItemList il, char* key, uint16_t copy){
    //Info(il->l, "getting '%s'", key);
    Node cur_node = il->head;
    while(cur_node != NULL){
        if (strcmp(cur_node->key, key) == 0){
            if (copy){
                char* val_copy = malloc(strlen(cur_node->value) + 1);
                strcpy(val_copy, cur_node->value);
                return val_copy;
            }
            return cur_node->value;
        }
        cur_node = cur_node->next; // step
    }

    // not found
    Info(il->l, "no such key '%s'", key);
    return NULL;
}

/**
 * @brief helper function for debug-printing a given node and its adjacent nodes
 * 
 * @param l logger to print to
 * @param n node to print
 */
void debug_cur_node_status(Logger l, Node n){
Debug(l, "node status: {<-%p|%p|%p->}{<-%p|%p|%p->}{<-%p|%p|%p->}",
                (n->prev == NULL?NULL:n->prev->prev),(n->prev == NULL?NULL:n->prev),(n->prev == NULL?NULL:n->prev->next),
                n->prev, n, n->next,
                (n->next == NULL?NULL:n->next->prev),(n->next == NULL?NULL:n->next),(n->next == NULL?NULL:n->next->next));
}

uint16_t ItemList_Del(ItemList il, char* key){
    //Info(il->l, "deleting '%s'", key);
    Node cur_node = il->head;
    while(cur_node != NULL){
        if(strcmp(cur_node->key, key) == 0){
            debug_cur_node_status(il->l, cur_node);
            // attach adjacent nodes
            if (cur_node->prev != NULL){
                cur_node->prev->next = cur_node->next;
            }
            if (cur_node->next != NULL){
                cur_node->next->prev = cur_node->prev;
            }

            // check if head or tail
            if(cur_node == il->head) il->head = cur_node->next;
            if(cur_node == il->tail) il->tail = cur_node->prev;

            // junk the node
            freeNode(cur_node);
            il->length -= 1;
            return 1;
        }
        cur_node = cur_node->next; // step
    }

    // not found
    Info(il->l, "no such key '%s'", key);
    return 0;
}

uint32_t ItemList_Append(ItemList il, char* restrict key, char* restrict value){
    //Info(il->l, "appending '%s' -> '%s'", key, value);

    // create a new node on the tail
    Node n = newNode(key, value, il->tail, NULL);
    
    if (il->length == 0 && il->head == NULL && il->tail == NULL){ // first node handling
        Debug(il->l, "first node append");
        il->head = n;
        il->tail = n;
        return (il->length += 1);
    }
    else if(il->length == 0){ // if length is zero, but head+tail aren't nil, we have a mismatch
        Warn(il->l, "ItemList length is 0 but head(%p)/tail(%p) are not nil ",il->head, il->tail);
    }

    // other nodes exist
    il->tail->next = n; // set cur tail's next
    n->prev = il->tail; // set new node's prev
    il->tail = n; // assign new node as tail
    

    return (il->length += 1);
}

uint32_t ItemList_Length(ItemList restrict il){
    return il->length;
}

char** ItemList_Dump(ItemList restrict il){
    char** lines = malloc(sizeof(char*)*il->length);

    // iterate through the list, one line per node
    Node cur_node = il->head;
    for (uint32_t i = 0; i < il->length; i++){
        if (cur_node == NULL){ // if length property <> actual length, we have an issue
            Fatal(il->l, "ItemList length (%d) <> actual node chain count (index: %d). ", il->length, i);
            return NULL;
        }

        lines[i] = stringNode(cur_node); // performs the malloc and stringifcation for us
        cur_node = cur_node->next; // advance
    }

    return lines;
}