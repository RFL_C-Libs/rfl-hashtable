
#include "itemlist.h"
#include "logger.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(){
    // generate a 6 item long list of items with unique keys
    ItemList il = NewItemListWithLogger(NULL, LOGLEVELDEBUG);
    ItemList_Append(il, "Hello", "World");
    ItemList_Append(il, "Hello2", "World");
    char* value_for_spacey_kv = "val  ue";
    ItemList_Append(il,"  key  ",value_for_spacey_kv);
    char* value_for_long_kv = "a much much much much much much much much longer key";
    ItemList_Append(il,"a much much much much much much much much longer key",
                        value_for_long_kv);
    char* value_for_empty_key = "value for empty key";
    ItemList_Append(il, "", value_for_empty_key);
    ItemList_Append(il, "key for empty value", "");
    
    // fetching values
    char* get_helloworld_copy = ItemList_Get(il,"Hello", 1); // this value must be freed
    printf("get_helloworld_copy: '%s' | expecting: '%s'\n", get_helloworld_copy, "World");
    free(get_helloworld_copy);
    
    char* get_helloworld_no_copy = ItemList_Get(il, "Hello2", 0); // this value should not be
    printf("get_helloworld_no_copy: '%s' | expecting: '%s'\n", get_helloworld_no_copy, "World");

    char* get_spacey_kv = ItemList_Get(il,"  key  ",1);
    printf("get_spacey_kv: '%s' | expecting: '%s'\n", get_spacey_kv, value_for_spacey_kv);
    free(get_spacey_kv);

    char* get_long_kv = ItemList_Get(il,"a much much much much much much much much longer key",1);
    printf("get_long_kv: '%s' | expecting: '%s'\n", get_long_kv, value_for_long_kv);
    free(get_long_kv);

    char* get_empty_key_copy = ItemList_Get(il,"",1);
    printf("get_empty_key_copy: '%s' | expecting: '%s'\n", get_empty_key_copy, value_for_empty_key);
    free(get_empty_key_copy);

    char* get_empty_key_no_copy = ItemList_Get(il,"",0);
    printf("get_empty_key_no_copy: '%s' | expecting: '%s'\n", get_empty_key_no_copy, value_for_empty_key);
    //free(get_empty_key_no_copy);

    char** dumped = ItemList_Dump(il);
    for (uint16_t i = 0; i < ItemList_Length(il); i++){
        printf("%s\n", dumped[i]);
        free(dumped[i]);
    }
    free(dumped);

    printf("Deleted Hello2: %d\n", ItemList_Del(il, "Hello2"));

    char** dumped2 = ItemList_Dump(il);
    for (uint16_t i = 0; i < ItemList_Length(il); i++){
        printf("%s\n", dumped2[i]);
        free(dumped2[i]);
    }
    free(dumped2);

    printf("Deleted Hell0: %d\n", ItemList_Del(il, "Hell0"));

    printf("Deleted tail ('key for empty value'): %d\n", ItemList_Del(il, "key for empty value"));
    printf("Expected length: 4 | Actual length: %d\n", ItemList_Length(il));
    char** dumped3 = ItemList_Dump(il);

    for (uint16_t i = 0; i < ItemList_Length(il); i++){
        printf("%s\n", dumped3[i]);
        free(dumped3[i]);
    }
    free(dumped3);

    FreeItemList(il,1);

}