/* A personal implementation of a hashtable in POSIX C. 

rflandau */
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <crypt.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include "logger.h"
#include "hashtable.h"

#define DJB2_MAGIC 5381

// Hash Table Item #############################################################

typedef struct RFLHashItem {
    char* key;
    char* value;
} RFLHASHItem;
typedef RFLHASHItem* HashItem;

HashItem newHashItem(char* key, char* value){
    HashItem hi = malloc(sizeof(RFLHASHItem));

    hi->key = malloc(strlen(key)+1); //include \0
    strcpy(hi->key, key);

    hi->value = malloc(strlen(value)+1);
    strcpy(hi->value, value);

    return hi;
}

void freeHashItem(HashItem hi){
    free(hi->key);
    free(hi->value);
    free(hi);
    hi = NULL;
}

// Hashtable ###################################################################

typedef struct RFLHashObj {
    uint32_t capacity; /**< max capacity */
    uint32_t count; /**< current item count */
    HashItem* items; /**< array of all K-Vs, indexed on hash % cap */

    // optional
    Logger l; /**< internal RFLLogger*/
} RFLHashObj;

// CONSTRUCTORS ----------------------------------------------------------------

/**
 * @brief initialize and return a new hashtable
 * 
 * allocates memory enough for the entire hashtable. 
 *
 * @param cap max number of elements in the hashtable
 * @return Hash 
 */
Hash initHash(uint32_t cap){
    Hash h; h = malloc(sizeof(RFLHashObj));
    h->capacity = cap;
    h->count = 0;

    // allocate space for the capacity
    h->items =  calloc(h->capacity,  sizeof(HashItem));

    return h;
}

Hash NewHash(uint32_t cap){
    Hash h = initHash(cap);
    h->l = NULL;
    return h;
}

Hash NewHashWithLogger(uint32_t cap, char* log_output_path, enum level lev){
    Logger l = NewLoggerWithLevel(log_output_path, lev);
    SetLevel(l, lev);

    Hash h = initHash(cap);
    Debug(l, "hash cap: %d", h->capacity);
    h->l = l;
    return h;
}

// DESTRUCTOR ------------------------------------------------------------------
void FreeHash(Hash h){
    FreeLogger(h->l);

    uint32_t freed_count = 0;
    for (uint32_t i = 0; i < h->capacity && freed_count < h->count; i++) {
        if (h->items[i] != NULL){
            freeHashItem(h->items[i]);
            freed_count += 1;
        }
    }
    free(h->items);
    free(h);
    h = NULL;
    return;
}

// METHODS #####################################################################

// internal only ---------------------------------------------------------------

/* hash returns the hashed value of the given key.
Uses the DJB2 hash function. */
uint64_t hash(char* key){
    uint64_t hash = DJB2_MAGIC;
    uint64_t cur_char; // we are using the char's decimal ACSII rep

    // range over the characters in the key
    while ((cur_char = (uint64_t)*key++)){ 
        // bitshift the hash, then add it back (the 31st and 32nd product)
        hash = ((hash << 5) + hash);
        // add in the variation (in the form of the key's character)
        hash += cur_char;
    }

    return hash;
}

// Fetches the HashItem containing the given key else NULL
HashItem internalGet(Hash h, char* restrict key){
    if (h->l != NULL) Debug(h->l, "internal get '%s'", key);
    // hash the key
    uint64_t h_key = hash(key); if (h->l != NULL) Debug(h->l, "---hashed '%ld'", key, h_key);
    HashItem i = h->items[h_key % h->capacity]; if (h->l != NULL) Debug(h->l, "---fetched item '%p'", i);
    if (i == NULL) return NULL; // unknown key

    if (strcmp(i->key, key) == 0) return i; // equality

    // collision
    // TODO implement chaining
    if (h->l != NULL){
        Warn(h->l, "Collision: given key: '%s', known key: '%s' (hash '%s', index: '%d')", 
            key, 
            i->key, 
            h_key, 
            h->items[h_key % h->capacity]);
    }
    printf("GET; collision!"); // TODO
    return NULL;
}

/**
 * @brief Get the number of digits in a given uint16
 * 
 * @param i 
 * @return u_int16_t 
 */
uint16_t getDigits(uint64_t i){
    uint16_t digits = 0;
    if (i == 0) return 1;
    while (i > 0){
        i /= 10;
        digits += 1;
    }

    return digits;
}


// Fields ----------------------------------------------------------------------

char* Add(Hash h, char* key, char* val){
    if (h->l != NULL) Debug(h->l, "add '%s' -> '%s'", key, val);
    if (Get(h, key) == NULL){ // item does not exist in hash
        // generate the item index
        uint64_t h_key = hash(key);
        Debug(h->l, "inserting new value at items[%d]", h_key % h->capacity);
        // generate the item and add it to the array
        h->items[h_key % h->capacity] = newHashItem(key,val);
        h->count += 1;
        return NULL;
    }

    //check if the key already exists in the hash
    // TODO implement chaining
    return "key already exists in the hash";
}

uint16_t Del(Hash h, char* restrict key){
    HashItem hi = internalGet(h, key);

    // confirm the key exists
    if (hi == NULL){
        return 0;
    }

    h->count -= 1;
    // free the hashitem and point the array to NULL
    freeHashItem(hi);
    
    return 1;
}

char* Get(Hash h, char* restrict key){
    HashItem hi = internalGet(h, key);

    if (hi == NULL) return NULL;
    return hi->value;
}

char** Dump(Hash h){
    if (h->l != NULL) Debug(h->l, "dump. cap: %d, count: %d", h->capacity, h->count);
    
    char** lines = malloc(sizeof(char*)*h->capacity);
    
    
    // loop through the underlying array 
    for (uint32_t i = 0; i < h->capacity; i++) {
        uint16_t digits = getDigits(i);
        HashItem hi = h->items[i];

        if (hi == NULL){
            // allocate for the line
            uint16_t bytes = digits + sizeof(": NULL") + 1; // + ": NULL" + \0
            
            if (h->l != NULL) Debug(h->l, "--- mallocing %d bytes", bytes);
            lines[i] = malloc(bytes);
            snprintf(lines[i], bytes, "%d: NULL", i);
            if (h->l != NULL) Debug(h->l, "--- constructed line: '%s'", lines[i]);
        }
        else{
            uint64_t h_key = hash(hi->key);
            uint16_t digits_in_hash = getDigits(h_key);
            
            uint64_t bytes = digits + sizeof(": hash: '") +
                                digits_in_hash + sizeof("' k: '") +
                                strlen(hi->key) + sizeof("' -> v: '") +
                                strlen(hi->value) + sizeof("'") + 1; // include null terminator
            if (h->l != NULL) Debug(h->l, "--- mallocing digits: %d+%d+%d+%d+%d+%d+%d+%d+%d=%d bytes",
                                    digits, 
                                    sizeof(": hash: '"), 
                                    digits_in_hash, 
                                    sizeof("' k: '"),
                                    strlen(hi->key), 
                                    sizeof("' -> v: '"), 
                                    strlen(hi->value),  sizeof("'"), 1, bytes);

                
            lines[i] = malloc(bytes);
            snprintf(lines[i], bytes, "%d: hash: '%ld' k: '%s' -> v: '%s'", i, h_key, hi->key, hi->value);
            if (h->l != NULL) Debug(h->l, "--- constructed line: '%s'", lines[i]);
        } 

        // sanity check
        if (lines[i] == NULL){
            if (h->l != NULL) {Fatal(h->l,"--- NULL dump line. i: %d | h->capacity: %d | h->items[i]: %s", i, h->capacity, h->items[i]);}
            return NULL;
        }
    }
    return lines;
}